const express = require('express');
const cors = require('cors')

//Create an app
const app = express();

app.use(cors({
  origin: '*',
  optionsSuccessStatus: 200 // some legacy browsers (IE11, various SmartTVs) choke on 204
}));

app.get('/api/v1/healthcheck', (req, res) => {
  res.send("OK");
});

app.get('/api/v1/hello', (req, res) => {
    res.json({ welcomeMsg: 'Phew...It worked!!! You did it!!!\n'});
});

//Listen port
const PORT = process.env.PORT || 9000;
app.listen(PORT, function() {
  console.log(`Running on port ${PORT}`);
});

