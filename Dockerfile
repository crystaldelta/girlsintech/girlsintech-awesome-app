FROM node:14-alpine

# Create app directory
WORKDIR /usr/src/app

# Copy package.json and package-lock.json to /app
COPY package*.json ./   

# Install all dependencies
RUN npm install          

# Copy the rest of the code
COPY . .                

# Open desired port
EXPOSE 9000

# Use js files to run the application
ENTRYPOINT ["node", "server.js"]