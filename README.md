# girlsintech-awesome-app

## Prerequisites

Check weather you have docekr installed on your meashine
```sh
$ docker --version
```

If you dont have docker installed, follow this link to install for your meashine. (https://docs.docker.com/engine/install/)

## Build and Push the Docker image to ECR repository

1. Export your AWS Profile
```sh
$ export AWS_PROFILE=cd-sandbox
```
2. Retrieve an authentication token and authenticate your Docker client to your registry.
```sh
aws ecr get-login-password --region ap-southeast-2 | docker login --username AWS --password-stdin 712322575905.dkr.ecr.ap-southeast-2.amazonaws.com
```
3. Build your Docker image (Make sure you are in the application repo)
```sh
docker build -t express-test .
```
4. After the build completes, tag your image so you can push the image to this repository:
```sh
docker tag express-test:latest 712322575905.dkr.ecr.ap-southeast-2.amazonaws.com/express-test:latest
```
5. Run the following command to push this image to your newly created AWS repository:
```sh
docker push 712322575905.dkr.ecr.ap-southeast-2.amazonaws.com/express-test:latest
```
